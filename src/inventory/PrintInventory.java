/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package inventory;

/**
 *
 * @authorAlejandro Arreola
 */
package inventory;

public class PrintInventory {
	
	  /**
     * A method to print full inventory i.e.item's ID, name and it's quantity.
     * @param inventory
     */
    public void printInventory(Item []inventory,int itemCounter) {
        for (int i = 0; i < itemCounter; i++) {
            System.out.println("ID: " + inventory[i].itemID
                    + "\t Name: " + inventory[i].getName()
                    + "\t Quantity:" + inventory[i].getQuantity());
        }
    }